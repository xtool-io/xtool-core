package xtool.core.impl

import com.google.common.io.ByteStreams
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.DefaultResourceLoader
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.stereotype.Service
import org.springframework.util.Assert
import xtool.core.api.FS
import java.io.StringWriter
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

@Service
class FSImpl(private val velocityEngine: VelocityEngine,
             private val log: Logger) : FS {

    override fun copyResource(srcResource: String, dest: Path, vars: Map<String, Any?>) {
        val resourceLoader = DefaultResourceLoader()
        val resource = resourceLoader.getResource("""templates/${srcResource.removePrefix("/")}""")
        val data = read(resource, vars)
        write(dest, data)
        log.info("[CREATE] {}", dest)
    }

    override fun copyResources(srcRootResource: String, dest: Path, vars: Map<String, Any?>) {
        val resolver = PathMatchingResourcePatternResolver()
        val srcPattern = """classpath:/templates/${srcRootResource.removePrefix("/").removeSuffix("/")}/**/*.*"""
        val resources = resolver.getResources(srcPattern)
        log.debug("{} arquivos encontrados no classepath={}, srcRootResource={}", resources.size, srcPattern, srcRootResource)
        for (resource in resources) {
            val data = read(resource, vars)
            val destFrag = inlineTemplate(URLDecoder.decode(resource.url.path, StandardCharsets.UTF_8.toString()).substringAfter(srcRootResource).removeSuffix(".vm").removePrefix("/"), vars)
            log.debug("DestFrag: {}", destFrag)
            write(dest.resolve(destFrag), data)
            log.info("[CREATE] {}", dest.resolve(destFrag))
        }
    }

    override fun mkdirs(path: Path): Path {
        if (Files.notExists(path)) {
            val newPath = Files.createDirectories(path)
            log.info("[CREATE] {}", path)
            return newPath
        }
        return path
    }

    fun inlineTemplate(template: String, vars: Map<String, Any?>): String {
        val velocityContext = VelocityContext(vars);
        val stringWriter = StringWriter();
        this.velocityEngine.evaluate(velocityContext, stringWriter, String(), template);
        return stringWriter.toString();
    }


    private fun read(resource: Resource, vars: Map<String, Any?>): ByteArray {
        log.debug("Lendo arquivo: {}", URLDecoder.decode(resource.url.path, StandardCharsets.UTF_8.toString()))
        val data: ByteArray = ByteStreams.toByteArray(resource.inputStream)
        if (resource.filename?.endsWith(".vm")!!) {
            val velocityContext = VelocityContext(vars)
            val stringWriter = StringWriter()
            this.velocityEngine.evaluate(velocityContext, stringWriter, String(), String(data))
            return stringWriter.toString().toByteArray(StandardCharsets.UTF_8)
        }
        return data
    }

    private fun write(dest: Path, data: ByteArray): String {
        log.debug("Escrevendo no destino: {} ", dest)
        if (Files.notExists(dest.parent)) Files.createDirectories(dest.parent)
        val os = Files.newOutputStream(dest)
        os.write(data)
        os.flush()
        os.close()
        return dest.toString()
    }

}
