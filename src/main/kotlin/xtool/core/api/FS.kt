package xtool.core.api

import java.nio.file.Path

/**
 * Interface com métodos com operações avancadas em filesystem.
 */
interface FS {
    /**
     * Realiza uma cópia da origem [srcResource] para o destino [dest] substituindo as variável [vars] do arquivo de origem.
     * É usando o Apache Velocity para a templatização de arquivos.
     */
    fun copyResource(srcResource: String, dest: Path, vars: Map<String, Any?>)

    /**
     * Realiza uma cópia lendo os arquivos recursivamente a partir da raiz srcRootResource.
     */
    fun copyResources(srcRootResource: String, dest: Path, vars: Map<String, Any?>)

    /**
     * Cria uma estrutura de diretório no caminho [path] especificado.
     */
    fun mkdirs(path: Path): Path
}
