package xtool.core

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class XtoolCoreModule {
    companion object {
        const val XTOOL_DEFAULT_GROUP = "Xtool Commands"
    }
}
