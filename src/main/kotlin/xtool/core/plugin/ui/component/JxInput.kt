package xtool.core.plugin.ui.component

import xtool.core.plugin.ui.PluginComponent
import java.awt.Component
import javax.swing.BoxLayout
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JTextField

class JxInput(
        id: String,
        label: String,
        enabled: (Map<String, Any>) -> Boolean = { true },
        options: Map<String, Any> = mapOf("columns" to 30)) : PluginComponent(id, label, enabled, options) {

    init {
        this.layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
        this.alignmentX = Component.LEFT_ALIGNMENT
        val jLabel = JLabel()
        jLabel.text = this.label
        val jTextField = JTextField(this.options["columns"] as Int)
        this.add(jLabel)
        this.add(jTextField)
    }
}
