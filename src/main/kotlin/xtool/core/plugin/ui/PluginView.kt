package xtool.core.plugin.ui

import xtool.core.plugin.XtoolPlugin
import java.awt.BorderLayout
import java.awt.Component
import javax.swing.*
import javax.swing.border.EmptyBorder

class PluginView(val plugin: XtoolPlugin) : JFrame() {
    val bodyPanel = JPanel()

    init {
        this.contentPane.layout = BorderLayout()
        this.title = plugin.name
        val contentPanel = JPanel()
        contentPanel.layout = BoxLayout(contentPanel, BoxLayout.PAGE_AXIS)
        contentPanel.border = EmptyBorder(5, 5, 5, 5)
        bodyPanel.layout = BoxLayout(bodyPanel, BoxLayout.PAGE_AXIS)

        plugin.onInit(this)

        // FOOTER
        val footerPanel = JPanel()
        footerPanel.layout = BoxLayout(footerPanel, BoxLayout.PAGE_AXIS)
        footerPanel.alignmentX = Component.LEFT_ALIGNMENT
        val submitButton = JButton("Ok")
        submitButton.addActionListener { plugin.onSubmit(HashMap()) }
        footerPanel.add(submitButton)

        contentPanel.add(bodyPanel, BorderLayout.CENTER)
        contentPanel.add(footerPanel, BorderLayout.SOUTH)
        this.contentPane.add(contentPanel)
        this.pack()
        this.setLocationRelativeTo(null)
        this.isFocusable = true
    }
}
