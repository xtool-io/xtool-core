package xtool.core.plugin.ui

import javax.swing.JPanel

open class PluginComponent(
        val id: String,
        val label: String,
        val enabled: (params: Map<String, Any>) -> Boolean,
        val options: Map<String, Any>) : JPanel()

