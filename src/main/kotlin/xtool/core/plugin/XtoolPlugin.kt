package xtool.core.plugin

import xtool.core.plugin.ui.PluginView

/**
 * Interface que representa um plugin Xtool.
 */
abstract class XtoolPlugin(val name: String, val description: String) {
    /**
     * Função chamada para a montagem da interface do usuário. Os componentes são adicionados ao [content].
     *
     */
    abstract fun onInit(view: PluginView)

    /**
     * Função chamada após o click no button de ok do plugin.
     */
    abstract fun onSubmit(params: Map<String, Any>)
}
