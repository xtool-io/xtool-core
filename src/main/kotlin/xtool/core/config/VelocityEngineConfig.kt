package xtool.core.config

import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.exception.VelocityException
import org.apache.velocity.runtime.RuntimeConstants
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.IOException


@Configuration
class VelocityEngineConfig {

    @Bean
    @Throws(VelocityException::class, IOException::class)
    fun getVelocityEngine(): VelocityEngine? {
        val ve = VelocityEngine()
        ve.setProperty("resource.loader", "class")
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath")
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader::class.java.name)
        ve.init()
        return ve
    }

}
