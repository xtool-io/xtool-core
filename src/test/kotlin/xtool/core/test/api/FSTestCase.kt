package xtool.core.test.api

import com.google.common.jimfs.Configuration
import com.google.common.jimfs.Jimfs
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import xtool.core.XtoolCoreModule
import xtool.core.api.FS
import java.nio.file.Files


@SpringBootTest(classes = [XtoolCoreModule::class])
class FSTestCase(@Autowired val fs: FS) {

    @Test
    fun copyResourceTest() {
        val fileSystem = Jimfs.newFileSystem(Configuration.unix())
        val foo = fileSystem.getPath("/foo")
        Files.createDirectory(foo)

        val vars = mapOf("name" to "Fulano")

        fs.copyResource("hello.txt.vm", foo.resolve("hello.txt"), vars)

        Assertions.assertTrue(Files.exists(foo.resolve("hello.txt")))
        Assertions.assertEquals("Hello: Fulano", String(Files.readAllBytes(foo.resolve("hello.txt"))).trim())

    }

    @Test
    fun copyResourcesTest() {
        val fileSystem = Jimfs.newFileSystem(Configuration.unix())
        val foo = fileSystem.getPath("/foo")
        Files.createDirectory(foo)

        val vars = mapOf(
                "name" to "Fulano",
                "package" to "br/jus")

        fs.copyResources("v1/app", foo, vars)

        println(foo.resolve(""))

        Assertions.assertTrue(Files.exists(foo.resolve("src/br/jus/hello.txt")))
        Assertions.assertEquals("Fulano", String(Files.readAllBytes(foo.resolve("src/br/jus/hello.txt"))).trim())
        Assertions.assertTrue(Files.exists(foo.resolve("pom.xml")))
    }

    @Test
    fun mkdirsTest() {
        val fileSystem = Jimfs.newFileSystem(Configuration.unix())
        val foo = fileSystem.getPath("/foo")
        Files.createDirectory(foo)
        val p = fs.mkdirs(foo.resolve("project1"))
        Assertions.assertTrue(p.equals(foo.resolve("project1")))
    }
}
