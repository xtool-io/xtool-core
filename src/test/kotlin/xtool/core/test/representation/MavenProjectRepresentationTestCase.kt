package xtool.core.test.representation

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.core.representation.MavenProjectRepresentation
import java.nio.file.Paths

class MavenProjectRepresentationTestCase {

    @Test
    fun getNameTest() {
        val mavenProject = MavenProjectRepresentation.of(Paths.get("src/test/resources/examples/demo"))
        Assertions.assertTrue(mavenProject.name.equals("demo"))
    }
}
